import { PayloadAction, createSlice } from "@reduxjs/toolkit";

// interface ThemeState {
//     dark: boolean;
// }

// const initialState: ThemeState = {
//     dark: true,
// };

// const themeSlice = createSlice({
//     name: 'theme',
//     initialState,
//     reducers: {
//         toggleDarkMode: (state) => {
//             state.dark = !state.dark
//         }
//     }
// })

// export const { toggleDarkMode } = themeSlice.actions;
// export default themeSlice.reducer;

type ThemeState = {
    dark:boolean
}

type PayloadActionState = {
    mode:'light'|'dark'|'system'
}

const initialState:ThemeState = {
    dark:true
}
const themeSlice = createSlice({
    name:'theme',
    initialState,
    reducers:{
        toggleDarkMode:(state,action:PayloadAction<PayloadActionState>)=>{
            if(action.payload.mode === 'light'){
                state.dark = false;
            }
            else if(action.payload.mode === 'dark'){
                state.dark = true;
            }
        }
    },

})

export const { toggleDarkMode } = themeSlice.actions;                       
export default themeSlice.reducer;