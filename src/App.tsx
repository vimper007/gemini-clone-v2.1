import { Route, Routes } from 'react-router-dom'
import Sidebar from './components/sidebar/sidebar'
import Gemini from './components/main-window/gemini'
import './App.scss'

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Gemini />}></Route>
        <Route path='/side' element={<Sidebar />}></Route>
      </Routes>
    </>
  )
}

export default App
