import Header from '../header/header'
import InfiniteScroller from './infinite-scroller'

type Props = {}

const MainWindow = (props: Props) => {
    return (
        <div className='flex-1 bg-gemini-main_container-background'>
            <Header/>
            <InfiniteScroller/>
        </div>
    )
}

export default MainWindow