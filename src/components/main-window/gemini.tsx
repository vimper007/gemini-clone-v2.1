import Sidebar from '../sidebar/sidebar'
import MainWindow from './main-window'

type Props = {}

const Gemini = (props: Props) => {
  return (
    <div className='w-full h-full flex'>
        <Sidebar/>
        <MainWindow/>
    </div>
  )
}

export default Gemini