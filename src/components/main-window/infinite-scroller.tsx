import React from 'react'
import UserQuery from './user-query'

type Props = {}

const InfiniteScroller = (props: Props) => {
  return (
    <div>
        <div className='mx-auto container py-5 max-w-[712px]'>
            <UserQuery/>
        </div>
    </div>
  )
}

export default InfiniteScroller