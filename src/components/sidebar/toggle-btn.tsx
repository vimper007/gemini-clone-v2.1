import { RootState } from '@/redux/store';
import { iconColor } from '@/utils/utils';
import { Menu } from 'lucide-react'
import { useSelector } from 'react-redux';
type Props = {
    dark?: boolean,
    onClick: () => void;
}

const ToggleBtn = ({ onClick }: Props) => {
    const dark = useSelector((state: RootState) => state.theme.dark)
    return (
        <span className="w-[48px] h-[48px] flex hover:bg-gemini-sidenav-toggle-hover rounded-full justify-center items-center cursor-pointer" onClick={onClick}>
            <Menu size={24} className='text-gemini-sidenav-color'/>
        </span>
    )
}

export default ToggleBtn