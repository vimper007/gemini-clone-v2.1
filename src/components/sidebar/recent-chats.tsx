import { Ellipsis, MessageSquare } from 'lucide-react'

type Props = {
}

const RecentChats = ({  }: Props) => {
    return (
        <div className='pl-[12px] py-1 mt-4 flex justify-between group hover:bg-gemini-sidenav-toggle-hover rounded-full'>
            <span className='flex items-center'>
                <MessageSquare size={18} className='mr-4 text-gemini-sidenav-color' />
                <p className='text-gemini-sidenav-color'>Test</p>
            </span>
            <span className='mr-2 flex items-center'>
                    <Ellipsis size={18} className='h-full w-full flex items-center opacity-0 group-hover:opacity-100 text-gemini-sidenav-color' />
            </span>
        </div>
    )
}

export default RecentChats