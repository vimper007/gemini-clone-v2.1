import { Plus } from "lucide-react"

type Props = {
}
const NewChatBtn = ({  }: Props) => {
    return (
        <div className=" ">
            <span className="flex py-2 px-3 bg-gemini-sidenav-newchat-background w-max rounded-full">
                <Plus className="mr-5 text-gemini-sidenav-color"  />
                <span className='text-gemini-sidenav-color'>New Chat</span>
            </span>
        </div>
    )
}

export default NewChatBtn