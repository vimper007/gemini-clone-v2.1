import { useState } from "react"
import NewChatBtn from "./new-chat-btn";
import ToggleBtn from "./toggle-btn";
import RecentChats from "./recent-chats";
import { textColor } from "@/utils/utils";
import { ModeToggle } from "./mode-toggle";

type Props = {}

const Sidebar = (props: Props) => {
  const [expanded, setExpanded] = useState(true);

  const sidebarWidth = expanded ? 'md:w-[278px] w-1/2' : 'w-[92px]';

  const handleToggle = () => {
    setExpanded(!expanded);
  };

  return (
    <div className={`${sidebarWidth} h-full bg-gemini-sidenav p-[14px] transition-width duration-300`}>
      <div className="pt-[12px] pl-[12px] mb-12">
        <ToggleBtn onClick={handleToggle} />
      </div>
      {expanded && (
        <>
          <div>
            <ModeToggle />
            <NewChatBtn />
          </div>
          <div className="mt-8">
            <p className={`font-semibold text-gemini-sidenav-color`}>Recent</p>
            <RecentChats />
          </div>
        </>
      )}
    </div>
  )
}

export default Sidebar