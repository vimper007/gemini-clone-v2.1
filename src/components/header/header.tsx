import React from 'react'
import GeminiVersionSelector from './gemini-version-selector-dropdown'

type Props = {}

const Header = (props: Props) => {
  return (
<div className='h-[86px] w-full bg-gemini-sidenav-toggle-hover flex-1 flex items-center'>
  <GeminiVersionSelector/>
</div>
  )
}

export default Header