export const iconColor = (dark: boolean) => {
    return dark ? '#fff' : '#000'
}
export const textColor = (dark: boolean) => {
    return dark ? 'text-white' : 'text-black'
}